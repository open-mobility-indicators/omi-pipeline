# Omi Pipeline

This project defines a CI pipeline that computes an indicator data and deploys the results on a static file server, and a mbtiles server. see [project architecture](https://gitlab.com/open-mobility-indicators/website/-/wikis/4_Mainteneur/devops#quelle-est-larchitecture-de-la-plateforme-)

The indicator itself has to be defined in another GitLab project and provide a container image that the pipeline will use.

## Usage

This pipeline is designed to be included from a GitLab project containing the source code of an indicator.

Look at the `.gitlab-ci.yml` file in any indicator source code template to see how to include this pipeline.

## Authorized indicator projects

The pipeline consumes resources on OMI infrastructure, so it is not available to any GitLab project.

The list of allowed projects are those found in [GitLab indicators group](https://gitlab.com/open-mobility-indicators/indicators).

## Pipeline jobs

![Pipeline definition diagram](./diagrams/pipeline-definition-diagram.png)
