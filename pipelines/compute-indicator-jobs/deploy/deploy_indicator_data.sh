#!/usr/bin/env bash
set -euo pipefail

function usage {
    echo "usage: $0 {source_dir} {target_dir}"
    exit 1
}

if [ "$#" -ne 2 ]; then
    usage
fi

SOURCE_DIR=$1
TARGET_DIR=$2

if [ ! -d $SOURCE_DIR ]; then
    echo "Source directory not found: $SOURCE_DIR"
    usage
fi

if [ ! -d $TARGET_DIR ]; then
    echo "Target directory not found: $TARGET_DIR"
    usage
fi

cp -a $SOURCE_DIR/* $TARGET_DIR