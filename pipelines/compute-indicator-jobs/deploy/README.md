# Deploy static

Generate `metadata.json` file from several parameters

## Install

Preferably use a virtual environment

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```
