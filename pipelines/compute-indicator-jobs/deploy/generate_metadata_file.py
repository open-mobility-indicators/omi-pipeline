#!/usr/bin/env python
#
# Generates metadata.json file v3 from command-line parameters
#
import argparse
import datetime
import json
import logging
import sys
from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Optional

from omi_api.infra.file_system.model import (
    JsonIndicatorV3,
    JsonParameterProfileV3,
    JsonSourceV3,
    MetadataJsonV3,
)

log = logging.getLogger(__name__)


def build_parameter_profile(
    parameter_profile_name: str, parameter_profile_file: Path
) -> Optional[JsonParameterProfileV3]:
    """Build optional parameter profile from parameter_profile_name and parameter profile file."""
    with parameter_profile_file.open("rt", encoding="UTF-8") as fdin:
        try:
            values = json.load(fdin)
        except JSONDecodeError:
            log.exception("Can't decode parameter profile JSON file")
            return None
        return JsonParameterProfileV3(name=parameter_profile_name, values=values)


def generate_metadata(
    *,
    created_at: datetime.datetime,
    slug: str,
    parameter_profile_name: str,
    parameter_profile_file: Path,
    source_project_url: str,
    source_branch_name: str,
    source_commit_id: str,
    pipeline_id: str,
) -> MetadataJsonV3:
    """Generate indicator metadata from parameters"""
    parameter_profile = build_parameter_profile(
        parameter_profile_name, parameter_profile_file
    )
    if parameter_profile is None:
        raise ValueError("Can't build parameter profile")

    source = JsonSourceV3(
        branch=source_branch_name,
        commit=source_commit_id,
        project_url=source_project_url,
    )

    indicator = JsonIndicatorV3(
        created_at=created_at,
        parameter_profile=parameter_profile,
        pipeline_id=pipeline_id,
        slug=slug,
        source=source,
    )

    return MetadataJsonV3(indicator=indicator)


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group("required arguments")
    required.add_argument(
        "--target_dir",
        type=Path,
        help="where to generate metadata JSON file",
        required=True,
    )
    required.add_argument(
        "--created_at",
        type=datetime.datetime.fromisoformat,
        help="indicator pipeline start time",
        required=True,
    )
    required.add_argument("--slug", type=str, help="indicator slug", required=True)
    required.add_argument(
        "--parameter_profile_name",
        type=str,
        help="indicator pipeline parameter profile name",
        required=True,
    )
    required.add_argument(
        "--parameter_profile_file",
        type=Path,
        help="indicator pipeline parameter profile JSON file",
        required=True,
    )
    required.add_argument(
        "--source_project_url",
        type=str,
        help="GitLab project url",
        required=True,
    )
    required.add_argument(
        "--source_branch_name",
        type=str,
        help="Git branch name",
        required=True,
    )
    required.add_argument(
        "--source_commit_id",
        type=str,
        help="Git commit ID",
        required=True,
    )
    required.add_argument(
        "--pipeline_id",
        type=str,
        help="GitLab CI pipepeline ID",
        required=True,
    )
    parser.add_argument("--log", default="INFO", help="level of logging messages")

    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    if not args.target_dir.is_dir():
        parser.error("Target dir not found: %r", str(args.target_dir))

    if not args.parameter_profile_file.is_file():
        parser.error(
            "Parameter profile file not found: %r", str(args.parameter_profile_file)
        )

    log.info("Building metadata dict...")
    metadata = generate_metadata(
        created_at=args.created_at,
        slug=args.slug,
        parameter_profile_name=args.parameter_profile_name,
        parameter_profile_file=args.parameter_profile_file,
        source_project_url=args.source_project_url,
        source_branch_name=args.source_branch_name,
        source_commit_id=args.source_commit_id,
        pipeline_id=args.pipeline_id,
    )

    log.info("Writing metadata file...")
    (args.target_dir / "metadata.json").write_text(
        metadata.json(indent=2, ensure_ascii=True, sort_keys=True)
    )

    log.info("Done.")


if __name__ == "__main__":
    sys.exit(main())
