#!/usr/bin/env bash
set -euo pipefail

if [ "$#" -ne 3 ]; then
    echo "usage: $0 {omi_api_reload_endpoint_url} {omi_api_reload_token} {reload_subject}"
    exit 1
fi

OMI_API_RELOAD_ENDPOINT_URL=$1
OMI_API_RELOAD_TOKEN=$2
RELOAD_SUBJECT=$3

RELOAD_URL="$OMI_API_RELOAD_ENDPOINT_URL?admin_token=$OMI_API_RELOAD_TOKEN&subject=$RELOAD_SUBJECT"

curl --fail --silent -X PUT $RELOAD_URL