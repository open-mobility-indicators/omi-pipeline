# check-indicator-metadata-py

TODO: update code url with real class

Validate `indicator.yaml` file against [Indicator metadata model](https://gitlab.com/open-mobility-indicators/raw-indicator-api/-/blob/ba9d7df945498bb797fe324391127df719f80efe/omi_api/domain/entities/catalog.py#L21).

## Install

Using a virtual env:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
