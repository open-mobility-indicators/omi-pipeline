#!/usr/bin/env
import argparse
import logging
import sys
from pathlib import Path

import yaml
from omi_api.infra.gitlab.model import GitLabIndicatorMetadata
from pydantic.error_wrappers import ValidationError

log = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "indicator_metadata_file", type=Path, help="indicator metadata file to validate"
    )
    args = parser.parse_args()

    if not args.indicator_metadata_file.is_file():
        parser.error(
            f"indicator metadata file not found: {str(args.indicator_metadata_file)!r}"
        )

    logging.basicConfig(level=logging.INFO, format="%(levelname)s:%(message)s")

    with args.indicator_metadata_file.open("rt", encoding="utf-8") as fd:
        try:
            yaml_obj = yaml.safe_load(fd)
            GitLabIndicatorMetadata.parse_obj(yaml_obj)
            log.info("File is valid.")
        except yaml.parser.ParserError:
            log.exception("File is not YAML valid")
            return 1
        except ValidationError:
            log.exception("File is not valid against indicator metadata model")
            return 1


if __name__ == "__main__":
    sys.exit(main())
