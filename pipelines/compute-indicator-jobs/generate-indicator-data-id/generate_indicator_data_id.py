#!/usr/bin/env python
import argparse
import sys

from nanoid import generate

ID_DEFAULT_SIZE = 21


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--size",
        type=int,
        default=ID_DEFAULT_SIZE,
        help=f"id size (default is {ID_DEFAULT_SIZE}",
    )
    args = parser.parse_args()

    print(generate(size=args.size))


if __name__ == "__main__":
    sys.exit(main())
